package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature", glue = { "stepDefinition" }, plugin = {
        "html:target/cucumber-report.html", "json:target/cucumber.json", "pretty:target/cucumber-pretty.txt",
        "junit:target/cucumber-results.xml" })

public class TestRunner {
}
