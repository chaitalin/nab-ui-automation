package context.browser;

import org.openqa.selenium.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.List;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BrowserManager implements Browser {

    private Properties prop;
    private WebDriver driver;

    public BrowserManager(Properties prop) {
        this.prop = prop;
        driver = this.load(prop.getProperty("browser"));
    }

    public WebDriver load(String name) {
        switch (name.toLowerCase()) {
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                return new FirefoxDriver();
            default:
                WebDriverManager.chromedriver().setup();
                return new ChromeDriver();
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void quitDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    public void open() {
        driver.manage().window().maximize();
        driver.get(prop.getProperty("url"));
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public void clickFromList(By list, String element) {
        List<WebElement> listOfElements = driver.findElements(list);
        for (WebElement ele : listOfElements) {
            if (ele.getText().contentEquals(element)) {
                ele.click();
                break;
            }
        }
        driver.navigate().refresh();
    }

    public WebElement getElement(By locator) {
        return driver.findElement(locator);
    }

    public void enterData(By locator, String text) {
        this.getElement(locator).clear();
        this.getElement(locator).sendKeys(text);
    }

    public void waitForElement(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(locator));
    }
}
