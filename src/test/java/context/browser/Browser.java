package context.browser;

import org.openqa.selenium.WebDriver;

public interface Browser {
    public void open();

    public WebDriver getDriver();

    public void quitDriver();
}
