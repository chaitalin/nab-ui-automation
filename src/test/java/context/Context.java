package context;

import java.util.Properties;
import context.browser.BrowserManager;

public interface Context {
    public Properties config();

    public BrowserManager browser();
}
