package context;

import java.util.Properties;
import context.browser.BrowserManager;

public class ContextManager implements Context {

    final private String propFileName = "test.config.properties";
    private BrowserManager browser;
    private Properties config;

    public ContextManager() {

        // Load config from properties file
        try {
            config = new ConfigManager().load(propFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Get browser web driver for given broser
        browser = new BrowserManager(config);
    }

    public Properties config() {
        return config;
    }

    public BrowserManager browser() {
        return browser;
    }
}
