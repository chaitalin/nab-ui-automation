package webpage;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import context.Context;
import java.util.HashMap;
import java.util.Map;

public class CustomerSupport {

    private WebDriver driver;
    private Context context;

    public CustomerSupport(Context context) {
        this.context = context;
        driver = context.browser().getDriver();
        PageFactory.initElements(driver, this);
    }

    By firstNameField = By.id("field-page-Page1-aboutYou-firstName");
    By lastNameField = By.id("field-page-Page1-aboutYou-lastName");
    By stateField = By.className("css-zls501-placeholder react-select__placeholder");
    By phoneField = By.id("field-page-Page1-aboutYou-phoneNumber");
    By emailField = By.id("field-page-Page1-aboutYou-email");
    By submit = By.id("page-Page1-btnGroup-submitBtn");
    By notExistingCustomer = By.xpath("//label[2]");

    public void callBackForm(String firstName, String lastName, String state, String phone, String email) {

        // NOTE: Assuming "New Home Loan" from "Customer Assistance Directory" is
        // submitted
        driver.get("https://www.nab.com.au/common/forms/consumer-call-centre-request-a-callback");
        context.browser().waitForElement(firstNameField);

        // Add form data to hashmap
        context.browser().getElement(notExistingCustomer).click();

        HashMap<By, String> map = new HashMap<By, String>();
        map.put(firstNameField, firstName);
        map.put(lastNameField, lastName);
        map.put(phoneField, phone);
        map.put(emailField, email);

        for (Map.Entry<By, String> entry : map.entrySet()) {
            context.browser().enterData(entry.getKey(), entry.getValue());
        }

        // TODO: Select state
        // State isn't selected for now
        // It's a react select component

        // What was tried
        // - Actions to move element and then send keys
        // - JavascriptExecutor using class name
        // Unfortunatelly, above options didn't work for me :(
    }

    public void submitCallBackForm() {
        // NOTE: JS executor used as webdriver
        // click doesn't work here
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String jsScript = "document.getElementById('page-Page1-btnGroup-submitBtn').click();";
        js.executeScript(jsScript);
    }
}
