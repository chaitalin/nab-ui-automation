package webpage;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import context.Context;

public class HomeLoanEnquiry extends CustomerSupport {

    private WebDriver driver;
    private Context context;

    public HomeLoanEnquiry(Context context) {
        super(context);
        this.context = context;
        driver = context.browser().getDriver();
        PageFactory.initElements(driver, this);
    }

    By listOfLoans = By.xpath("//div[@class='textWithIconRed']//li//a");
    By listOfOptions = By.xpath("//div[@class='nabrwd-banner hidden-xs']//li//p");
    By radioButton = By.id("myRadioButton-0");

    // To click Home loans
    public String clickHomeLoans() {
        String elementToBeClicked = "Home loans";
        context.browser().clickFromList(listOfLoans, elementToBeClicked);
        return driver.getCurrentUrl();
    }

    // To click Enquire about a new loan
    public String clickEnquireNewLoan() {
        String elementToBeClicked = "Enquire about a new loan";
        context.browser().clickFromList(listOfOptions, elementToBeClicked);
        return driver.getTitle();
    }

    public void chooseNewHomeLoanFromCustomerAssistance(String enquiryOption) {
        // TODO: Choose new home loan from customer assistance page
        // ISSUE:
        // - iFrame from different source
        // - Chrome version >75 doesn't support executing
        // scripts for iFrames from different source

        // What was tried
        // - Tried to find iFrame for the page
        // - Tried to get path from ChroPath extension
        // - Tried to run JS from console
        //
        // Please see
        // - docs/home_loan_iframe_issue.png
        // - docs/home_loan_js_issue.png
    }
}
