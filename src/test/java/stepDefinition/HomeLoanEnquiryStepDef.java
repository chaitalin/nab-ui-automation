package stepDefinition;

import context.ContextManager;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import webpage.HomeLoanEnquiry;
import context.Context;

public class HomeLoanEnquiryStepDef {

    private Context context;
    private HomeLoanEnquiry homeLoan;

    public HomeLoanEnquiryStepDef(ContextManager context) {
        this.context = context;
    }

    @After()
    public void quitDriver() {
        context.browser().quitDriver();
    }

    @Given("I am on home page")
    public void i_am_on_home_page() {
        context.browser().open();
    }

    @Given("I click home loans")
    public void i_click_home_loans() {
        String expectedTitle = "https://www.nab.com.au/personal/home-loans";
        homeLoan = new HomeLoanEnquiry(context);
        Assert.assertEquals(expectedTitle, homeLoan.clickHomeLoans());
    }

    @When("I click enquire about a new home loan")
    public void i_click_enquire_about_a_new_loan() {
        String expectedTitle = "Customer assistance directory | We’re here to help - NAB";
        Assert.assertEquals(expectedTitle, homeLoan.clickEnquireNewLoan());
    }

    @Then("I select {string}")
    public void i_select(String enquiryOption) {
        homeLoan.chooseNewHomeLoanFromCustomerAssistance(enquiryOption);
    }

    @And("I fill contact form with {string} {string} {string} {string} {string}")
    public void i_fill_contact_form_with(String firstName, String lastName, String state, String phone, String email) {
        homeLoan.callBackForm(firstName, lastName, state, phone, email);
    }

    @And("I submit form")
    public void i_submit_form() {
        homeLoan.submitCallBackForm();
    }
}
