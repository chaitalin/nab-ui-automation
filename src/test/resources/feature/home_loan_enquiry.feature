@home-loan
Feature: Home load

  @home-loan-enquiry
  Scenario Outline: Home loan enquiry

    Given I am on home page
    And I click home loans
    When I click enquire about a new home loan
    Then I select "<enquiry_option>"
    And I fill contact form with "<first_name>" "<last_name>" "<state>" "<phone>" "<email>"
    And I submit form

    Examples:
      |enquiry_option   |first_name |last_name  |state  |phone      |email                      |
      |New home loan    |Jessica    |Jones      |NT     |0400000010 |jessica.jones@marvel.com   |
