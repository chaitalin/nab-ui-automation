# Nab customer support UI automation

## Prerequisite

- Java 1.8
- Maven 3.6
- Chrome 80
- Firefox 73

## Framework

- Testing approach - BDD

  - [Feature files](./src/test/resources/feature/)
  - [Step definitions](./src/test/java/stepDefinition)

- Browser automation - Selenium WebDriver
- BDD tool - Cucumber
- Test tool - JUnit
- Build tool - Maven
- Configuration management - [Properties file to change browser, host etc](./src/test/resources/test.config.properties)

## Usage

- Run all scenarios

      ```
      mvn clean test
      ```

## Reports

- HTML report

      ```
      target/cucumber-report.html
      ```

- Json report

      ```
      target/cucumber.json
      ```

- Scenario report

      ```
      target/cucumber-pretty.txt
      ```

- XML report

      ```
      target/cucumber-results.xml
      ```
